FROM node

WORKDIR /app

RUN apt-get update && apt-get install --yes git automake autoconf && \
  git clone https://github.com/facebook/watchman.git /tmp/watchman-src && \
  cd /tmp/watchman-src && \
  git checkout v4.7.0 && \
  ./autogen.sh && \
  ./configure --enable-statedir=/tmp --without-python --without-pcre && \
  make && \
  make install && \
  rm -r /tmp/watchman-src

RUN npm i -g expo-cli